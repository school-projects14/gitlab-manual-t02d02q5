Hello, dear user. Here we will create our own repository on GitLab and have fun!


## Creating a personal repository with the correct .gitignore and simple README.MD,

You can't create personl repo with School-21 gitlab, so you have to go to global [Gitlab](gitlab.com) register there using your school e-mail, and activate account via link in e-mail, which should be forwarded to your personal e-mail

After successfull login to account you can create new project

[create](pics/create.png)

Do not forget to copy you SSH key (from day 1 task) to your new GitLab (go to preferences)

Basic README.MD file will be present in your repo just from beginning and you could edit it with your favorite vim text editor
.gitignore you should add by yourself some sample files can be found here [Git Ignore Repo] (https://github.com/github/gitignore/blob/main) I have downloaded file for c language and added it to the repository

##Creating develop and master branches

You can do it from Gitlab interface, but it is better to practice and do it in terminal

git clone "your repo link"
git checkout -b master
git checkout -b develop

do not forget to push your changes to gitlab via

git push origin --all (or by pushing certain branch only via git push origin branch-name)

## Setting the develop branch as the default

You can set develop branch as default from gitlab interface - go to *Settings->Repositoty->Default branch* and set *develop* as a default one
![default](pics/default.png)

## Creating an issue for creating the current manual

It can be done from main project menu (panel on the left side)
![issue](pics/issui.png)

## creating a branch for the issue,

you can do it from gitlab web-interface, just look at this picture
![branch](pics/branch.png)

## creating a merge request on the develop branch,

after you have completed all work in feature branch you can create merge request
![merge](pics/merge.png)

## commenting and accepting the request,

user with necessary rights could review pull request and accept it, merging to the default branch (develop_

## creating a stable version in the master with a tag,

tags or stable releases could be created from deployment menu on panel
feel free to check yourself how to do it
![tag](pics/deploy.png)

## working with wiki for the project.

Good old vim could be used for compiling documentation like here, on picture below
Text can be formatted with markdown language
![vim](pics/vim.png)


So, you are a gitlab profi now, congrats!
